﻿using System;

namespace number2._2
{
    class Program
    {
        static void Main(string[] args)
        {
            bool t = true;
            for (int x = 0; x < 2; x++)
            {
                for (int y = 0; y < 2; y++)
                {
                    for (int z = 0; z < 2; z++)
                    {
                        bool c = Convert.ToBoolean(x);
                        bool d = Convert.ToBoolean(y);
                        bool e = Convert.ToBoolean(z);
                        if (t == (d || !e && c || !d))
                        {
                            Console.WriteLine($"{x} {y} {z}");
                        }
                    }
                }
            }
            Console.ReadKey();
        }
    }
}